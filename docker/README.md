# Docker images for the CI steps
Those Dockerfiles build images that contain base utilities for each stage.  
They allow for faster pipelines since expensive `yum install` calls can be skipped.

## Image location
The images are stored in the [GitLab registry associated with the repository](https://gitlab.cern.ch/network/ci-base-configs/container_registry).

## Updating the images
The images shouldn't require a lot of maintenance, but they may need to be updated from time to
time if a newer version of the packages that they contain is required.
To update all images, run the following commands:
```
docker login gitlab-registry.cern.ch
./build_all.sh
```