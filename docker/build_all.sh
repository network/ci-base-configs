#!/bin/sh

PREFIX='gitlab-registry.cern.ch/network/ci-base-configs'

for FILE in $(ls Dockerfile* | grep -v '.*~$'); do
    IMAGE_NAME="${PREFIX}/$(echo $FILE | cut -d- -f2)"
    echo "Building $IMAGE_NAME"
    docker build -t $IMAGE_NAME -f $FILE .
    docker push $IMAGE_NAME
done
