#!/usr/bin/env python3.6
"""
This module checks GitLab-CI files against the GitLab API to check if they
contain errors.
"""

from argparse import ArgumentParser
from http.client import HTTPSConnection, HTTPException
from json import dumps, loads, JSONDecodeError
from typing import List

PROJECT_ID = '46302'

IGNORE_ERRORS_BY_FILE = {
    'python36_monorepo_lib.yml': [
        'jobs config should contain at least one visible job',
    ]
}


def get_errors(filename: str, token: str, debug: bool) -> List[str]:
    """Get a list of errors for a GitLab-CI config file from the GitLab API."""
    with open(filename, mode='r', encoding="utf-8") as config_fd:
        payload = dumps({'content': config_fd.read()})

    conn = HTTPSConnection('gitlab.cern.ch')

    if debug:
        conn.set_debuglevel(1000)

    conn.request(
        method='POST',
        url=f'/api/v4/projects/{PROJECT_ID}/ci/lint',
        body=payload,
        headers={
            'Content-Type': 'application/json',
            'PRIVATE-TOKEN': token,
        }
    )

    res = conn.getresponse()
    try:
        json_res = loads(res.read())
    except JSONDecodeError as exc:
        raise HTTPException(f"API post failed: {res.status} {res.reason}") from exc
    if 'errors' not in json_res:
        raise HTTPException(f"API post failed with response: {json_res}")

    ignore_errors = IGNORE_ERRORS_BY_FILE.get(filename, [])
    errors = [e for e in json_res['errors'] if not any(e in i_e for i_e in ignore_errors)]

    return errors


def main() -> None:
    """Main function of the module."""
    parser = ArgumentParser('CI files linter')
    parser.add_argument(
        'ci_files',
        type=str,
        nargs='+',
        help='The GitLab-CI files to lint')

    parser.add_argument('-d', '--debug', action='store_true', default=False)
    parser.add_argument('--token')

    args = parser.parse_args()

    for ci_file in args.ci_files:
        errors = get_errors(ci_file, args.token, args.debug)

        for error in errors:
            print(f'{ci_file}: {error}')

        if errors:
            raise SystemExit(f'{ci_file} has errors.')

        print(f'{ci_file}: OK')


if __name__ == '__main__':
    main()
